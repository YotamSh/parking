﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class ParkingLot
    {
        private readonly Address parkingLotAddress;
        private readonly int parkingLotNumber;
        LinkedList<Order> orders;
        private bool[] parkingPlace;
        private readonly float pricePerHour;
        public ParkingLot(Address address, int id, LinkedList<Order> orders, bool[] parkingPlaces, float pricePerHour)
        {
            if (pricePerHour < 0)
                throw new ArgumentException("Price must be positive");
            if (id < 0)
                throw new ArgumentException("ID must be positive");
            this.parkingLotAddress = address;
            this.parkingLotNumber = id;
            this.parkingPlace = parkingPlaces;
            this.pricePerHour = pricePerHour;
            this.orders = new LinkedList<Order>(orders);
        }
        public LinkedList<Order> GetOrders() => this.orders;
        public bool[] GetParkingPlaces() => this.parkingPlace;
        public string GetAddress() => this.parkingLotAddress.ToString();
        public int GetParkingLotID() => this.parkingLotNumber;
        public Order GetOrderByCarNumber(string carNumber)
        {
            foreach(Order tmp in this.orders)
            {
                if (tmp.GetVehicleNumber() == carNumber)
                    return tmp;
            }
            throw new Exception("Car not found!");
        }
        public bool CanFulfill(Order order)
        {
            foreach(Order o in this.orders)
            {
                if (order.GetParkingPlace() == o.GetParkingPlace() && o.GetBiginningHour() <= order.GetBiginningHour())
                    return false;
            }
            return true;
        }
        public void AddOrder(Order order)
        {
            if (order.GetParkingPlace() > this.parkingPlace.Length)
                throw new ArgumentException("Iligal parking place");
            if (!this.CanFulfill(order))
                throw new Exception("Can't fulfill order");
            order.SetPrice(this.pricePerHour);
            this.orders.AddFirst(order);
        }
        public void FreeCar(string carNumber, int hour)
        {
            if (hour > 24 || hour < 0)
                throw new Exception("Time not in range!");
            Order tmp = this.GetOrderByCarNumber(carNumber);
            if (this.parkingPlace.Length < tmp.GetParkingPlace() || tmp.GetParkingPlace() < 0)
                throw new Exception("Iligal order - parking place not found");
            if (hour > tmp.GetEstimatedEndingHour())
                throw new Exception("Car not in parking lot");
            this.parkingPlace[tmp.GetParkingPlace()] = false;
            tmp.SetEndingHour(hour);
            tmp.DoOrder();
        }
        public float GetPrice(string carNumber, int hour)
        {
            if (hour > 24 || hour < 0)
                throw new Exception("Time not in range!");
            Order order = this.GetOrderByCarNumber(carNumber);
            if (order.GetBiginningHour() < hour && !order.GetDone())
                throw new Exception("Invalid order");
            if (order.GetEndingHour() != hour)
                order.SetEndingHour(hour);
            //order.DoOrder();
            return order.GetPricePerHour() * (order.GetEndingHour() - order.GetBiginningHour());
        }
        public void CancelNotFulfilled(int hour)
        {
            if (hour > 24 || hour < 0)
                throw new Exception("Time not in range!");
            LinkedList<Order> newOrders = new LinkedList<Order>();
            foreach(Order tmp in this.orders)
            {
                if (!(tmp.GetBiginningHour() > hour && !tmp.GetDone()))
                    newOrders.AddFirst(tmp);
            }
            this.orders = new LinkedList<Order>(newOrders);
        }
        /// <summary>
        /// updates the parking places in this
        /// </summary>
        /// <param name="hour">current time</param>
        public void UpdatePlaces(int hour)
        {
            if (hour > 24 || hour < 0)
                throw new Exception("Time not in range!");
            this.CancelNotFulfilled(hour);
            foreach(Order tmp in this.orders)
            {
                if(tmp.GetEndingHour() == hour && tmp.GetDone())
                {
                    this.FreeCar(tmp.GetVehicleNumber(), hour);
                }
                if(tmp.GetBiginningHour() == hour && !tmp.GetDone())
                {
                    this.parkingPlace[tmp.GetParkingPlace()] = true;
                    tmp.SetDone(true);
                }
            }
        }
    }
}
