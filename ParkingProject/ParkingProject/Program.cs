﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class Program
    {
        static int hour = 0;
        static T GetTFromConsole<T>(string message)
        {
            System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(T));
            if (converter == null)
                throw new Exception("Unable to parse this type");
            Console.Write(message);
            return (T)converter.ConvertFromString(Console.ReadLine());
        }
        static Address GetAddress()
        {
            string city = GetTFromConsole<string>("Enter city: ");
            string street = GetTFromConsole<string>("Enter street: ");
            try
            {
                uint number = GetTFromConsole<uint>("Enter house number: ");
                return new Address(street, city, number);
            }
            catch(Exception e)
            {
                Console.WriteLine("Invalid input!");
                return null;
            }
        }
        static User GetUser(bool withID)
        {
            string name = GetTFromConsole<string>("Enter name: ");
            Console.WriteLine("Enter address:");
            Console.WriteLine("Enter home address for {0}", name);
            Address address = GetAddress();
            if (address == null)
                return null;
            string id = "";
            if (withID)
                id = GetTFromConsole<string>("Enter id: ");
            return new User(name, id, address, 0);
        }
        static Order GetOrder(bool withUser)
        {
            string CarNumber = GetTFromConsole<string>("Enter car number: ");
            int startHour = GetTFromConsole<int>("Start hour: ");
            int endHour = GetTFromConsole<int>("End hour: ");
            uint place = GetTFromConsole<uint>("Enter parking place: ");
            User user = null;
            if (withUser)
            {
                Console.WriteLine("Get user ordering: ");
                user = GetUser(false);
            }
            if (withUser && user == null)
                return null;
            try
            {
                Order o = new Order(CarNumber, startHour, endHour, startHour, endHour, place, 0, user);
                return o;
            }catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
            return null;
        }
        static ParkingLot GetParkingLot()
        {
            LinkedList<Order> orders = new LinkedList<Order>();
            int id = GetTFromConsole<int>("Enter parking lot id: ");
            Console.WriteLine("Enter address for parking lot: ");
            Address address = GetAddress();
            if (address == null)
                return null;
            float price = GetTFromConsole<float>("Enter price per hour: ");
            int size = GetTFromConsole<int>("Enter size of parking lot: ");
            bool[] places = new bool[size];
            try
            {
                return new ParkingLot(address, id, orders, places, price);
            }catch(ArgumentException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        static ParkingLotCollection GetParkingLotCollection()
        {
            int lotsNumber = 0;
            try
            {
                lotsNumber = (int)GetTFromConsole<uint>("How many parking lots there are? ");
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
            ParkingLot[] parkingLots = new ParkingLot[lotsNumber];
            ParkingLot tmp = null;
            for(int i = 0; i < lotsNumber; i++)
            {
                Console.WriteLine("Enter lot number {0}", i + 1);
                tmp = GetParkingLot();
                if(tmp == null)
                {
                    Console.WriteLine("Invalid parking lot");
                    i--;
                    continue;
                }
                parkingLots[i] = tmp;
            }
            return new ParkingLotCollection(new LinkedList<User>(), parkingLots);
        }
        static void AddOrder(ParkingLotCollection parkingLotCollection)
        {
            string userID = GetTFromConsole<string>("Enter id for user: ");
            User thisUser = null;
            bool byID = true;
            ParkingLot lot = null;
            Order order = null;
            try
            {
                thisUser = parkingLotCollection.GetUserByID(userID);
            }
            catch (KeyNotFoundException e)
            {
                thisUser = GetUser(false);
                if (thisUser == null)
                    return;
                thisUser.SetID(userID);
                parkingLotCollection.AddUser(thisUser);
            }
            while (order == null)
            {
                order = GetOrder(false);
                if (order == null)
                    Console.WriteLine("Invalid order, try again!");
            }
            order.SetUser(thisUser);
            byID = GetTFromConsole<char>("Do you know the id of the parking lot?[Y\\N]").ToString().ToLower() == "y";
            try
            {
                if (byID)
                    lot = parkingLotCollection.GetLotByID(GetTFromConsole<int>("Enter id: "));
                else
                    lot = parkingLotCollection.GetByAddress(GetAddress());
            }catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.ReadKey();
            }
            if (lot != null)
            {
                if (lot.CanFulfill(order))
                {
                    Console.WriteLine("Order added to parking lot in address {0}", lot.GetAddress());
                    lot.AddOrder(order);
                }
                else
                    Console.WriteLine("Unable to add order to lot, parking place is taken");
            }
        }
        static void TimeChange(ParkingLotCollection parkingLotCollection)
        {
            hour = (hour + 1) % 25;
            parkingLotCollection.TimeCycle(hour);
        }
        static void FreeCar(ParkingLotCollection parkingLotCollection)
        {
            bool byID = false;
            ParkingLot lot = null;
            string carNumber = "";
            string userID = "";
            Order o = null;
            byID = GetTFromConsole<char>("Do you know the id of the parking lot?[Y\\N]").ToString().ToLower() == "y";
            try
            {
                if (byID)
                    lot = parkingLotCollection.GetLotByID(GetTFromConsole<int>("Enter id: "));
                else
                    lot = parkingLotCollection.GetByAddress(GetAddress());
            }catch(Exception e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
            if (lot != null)
            {
                carNumber = GetTFromConsole<string>("Enter car number: ");
                userID = GetTFromConsole<string>("Enter your id: ");
                try
                {
                    o = lot.GetOrderByCarNumber(carNumber);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.ReadKey();
                }
                if (o != null)
                {
                    if (o.GetUser().GetID() == userID)
                        lot.FreeCar(carNumber, hour);
                    else
                        Console.WriteLine("Not your car");
                }
            }
        }
        static void LotData(ParkingLotCollection parkingLotCollection)
        {
            ParkingLot[] parkingLots = parkingLotCollection.GetParkingLots();
            int tmpCounter = 0;
            for(int i = 0; i < parkingLots.Length; i++)
            {
                tmpCounter = 0;
                for (int j = 0; j < parkingLots[i].GetParkingPlaces().Length; j++)
                {
                    if (parkingLots[i].GetParkingPlaces()[j] == false)
                        tmpCounter++;
                }
                Console.WriteLine("Parking lot {0} has {1} free spaces in", parkingLots[i].GetParkingLotID(), tmpCounter);
            }
            Console.ReadKey();
        }
        static void UserHandle(ParkingLotCollection parkingLotCollection)
        {
            string id = GetTFromConsole<string>("Enter id: ");
            User u = null;
            try
            {
                u = parkingLotCollection.GetUserByID(id);
            }catch(KeyNotFoundException e)
            {
                Console.WriteLine(e);
                Console.ReadKey();
            }
            if (u != null)
            {
                Console.WriteLine("User {0} must pay {1}", u, u.GetPayment());
                bool toPay = char.ToLower(GetTFromConsole<char>("Would you like to pay now?[Y\\N]")) == 'y';
                if (toPay)
                {
                    // pay in some way
                    u.paying();
                }
            }
        }
        static void Main(string[] args)
        {
            ParkingLotCollection parkingLotCollection = GetParkingLotCollection();
            char option = '\0';
            const char ADD_ORDER = '1', FREE_CAR = '2', GET_DATA = '3', TIME = '4', USER = '5';
            do
            {
                Console.Clear();
                option = GetTFromConsole<char>("Time:" + hour.ToString() + "\nWhat would you like to do?(q to exit)\n1.Add order\n2.Free car from lot\n3.Print number of avaliable places in lots\n4.++time\n5.Get data of user\n");
                option = char.ToLower(option);
                switch(option)
                {
                    case ADD_ORDER:
                        AddOrder(parkingLotCollection);
                        break;
                    case FREE_CAR:
                        FreeCar(parkingLotCollection);
                        break;
                    case GET_DATA:
                        LotData(parkingLotCollection);
                        break;
                    case TIME:
                        TimeChange(parkingLotCollection);
                        break;
                    case USER:
                        UserHandle(parkingLotCollection);
                        break;
                    case 'q':
                        break;
                    default:
                        Console.WriteLine("Invalid option! Press any key to continue");
                        Console.ReadKey();
                        break;
                }
            } while (option != 'q');
        }
    }
}
