﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class Order
    {
        private static int OrderCounter = 0;
        private int orderNumber;
        private string vehicleNumber;
        private int estimatedBiginningHour;
        private int estimatedEndingHour;
        private int biginningHour;
        private int endingHour;
        private uint parkingPlace;
        private float pricePerHour;
        private bool done;
        private User user;
        public Order(string vehicleNumber, int estimatedBiginningHour, int estimatedEndingHour, int biginningHour, int endingHour, uint parkingPlace, float pricePerHour, User u)
        {
            if (estimatedBiginningHour > 24 || estimatedBiginningHour < 0
                || estimatedEndingHour > 24 || estimatedEndingHour < 0 ||
                biginningHour > 24 || biginningHour < 0
                || endingHour > 24 || endingHour < 0 || estimatedBiginningHour > estimatedEndingHour || biginningHour > endingHour)
                throw new ArgumentException("Iligal hours!");
            if (pricePerHour < 0)
                throw new ArgumentException("Price must be positive!");
            OrderCounter++;
            this.orderNumber = OrderCounter;
            this.vehicleNumber = vehicleNumber;
            this.estimatedBiginningHour = estimatedBiginningHour;
            this.estimatedEndingHour = estimatedEndingHour;
            this.biginningHour = biginningHour;
            this.endingHour = endingHour;
            this.parkingPlace = parkingPlace;
            this.pricePerHour = pricePerHour;
            this.done = false;
            this.user = u;
        }
        //gets
        public int GetOrderNumber()
        {
            return this.orderNumber;
        }
        public User GetUser() => this.user;
        public string GetVehicleNumber()
        {
            return this.vehicleNumber;
        }
        public int GetEstimatedBiginningHour()
        {
            return this.estimatedBiginningHour;
        }
        public float GetPricePerHour()
        {
            return this.pricePerHour;
        }
        public int GetEstimatedEndingHour()
        {
            return this.estimatedEndingHour;
        }
        public int GetBiginningHour()
        {
            return this.biginningHour;
        }
        public int GetEndingHour()
        {
            return this.endingHour;
        }
        public uint GetParkingPlace()
        {
            return this.parkingPlace;
        }

        public bool GetDone()
        {
            return this.done;
        }
        //sets
        public void SetOrderNumber(int orderNumber)
        {
            if (orderNumber < 0)
                throw new ArgumentException();
            this.orderNumber = orderNumber;
        }
        public void SetVehicleNumber(string vehicleNumber)
        {
            this.vehicleNumber = vehicleNumber;
        }
        public void SetEstimatedBiginningHour(int estimatedBiginningHour)
        {
            if (estimatedBiginningHour > 24 || estimatedBiginningHour < 0)
                throw new ArgumentException();
            this.estimatedBiginningHour = estimatedBiginningHour;
        }
        public void SetEstimatedEndingHour(int estimatedEndingHour)
        {
            if (estimatedEndingHour > 24 || estimatedEndingHour < 0)
                throw new ArgumentException();
            this.estimatedEndingHour = estimatedEndingHour;
        }
        public void SetEndingHour(int endingHour)
        {
            if (endingHour > 24 || endingHour < 0)
                throw new ArgumentException();
            this.endingHour = endingHour;
        }
        public void SetBiginningHour(int biginningHour)
        {
            if (biginningHour > 24 || biginningHour < 0)
                throw new ArgumentException();
            this.biginningHour = biginningHour;
        }

        public void SetUser(User thisUser)
        {
            this.user = thisUser;
        }

        public void SetParkingPlace(uint parkingPlace)
        {
            this.parkingPlace = parkingPlace;
        }
        public void SetDone(bool done)
        {
            this.done = done;
        }
        public void SetPrice(float price)
        {
            if(price <= 0)
                throw new ArgumentException("Price must be positive!");
            this.pricePerHour = price;
        }
        //functions
        public override string ToString()
        {
            return String.Format("Order number: {0}\nStart hour: {1}, end hour: {2}\nParking place: {3}\nVehicle number: {4}\nDone? {5}", this.orderNumber, this.biginningHour, this.endingHour, this.parkingPlace, this.vehicleNumber, this.done);
        }
        public static bool operator==(Order o1, Order o2)
        {
            if (ReferenceEquals(o1, o2))
                return true;
            if (o1 is null || o2 is null)
                return false;
            return o1.biginningHour == o2.biginningHour && o1.endingHour == o2.endingHour && o1.orderNumber == o2.orderNumber && o1.parkingPlace == o2.parkingPlace && o1.vehicleNumber == o2.vehicleNumber;
        }
        public static bool operator!=(Order o1, Order o2)
        {
            return !(o1 == o2);
        }
        public void DoOrder()
        {
            this.user.addPayment(this.pricePerHour * (this.endingHour - this.biginningHour));
        }
    }
}
