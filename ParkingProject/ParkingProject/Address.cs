﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class Address
    {
        private string streetName;
        private string city;
        private uint houseNumber;
        public Address(string streetName, string city, uint houseNumber)
        {
            this.streetName = streetName;
            this.city = city;
            this.houseNumber = houseNumber;
        }
        public string GetStreetName()
        {
            return this.streetName;
        }
        public string GetCity()
        {
            return this.city;
        }
        public uint GetHouseNumber()
        {
            return this.houseNumber;
        }
        public void SetStreetName(string newName)
        {
            this.streetName = newName;
        }
        public void SetCity(string newCity)
        {
            this.city = newCity;
        }
        public void SetNumber(uint newNumber)
        {
            this.houseNumber = newNumber;
        }
        public override string ToString()
        {
            return String.Format("City: {0}, Street: {1}, House number: {2}", this.city, this.streetName, this.houseNumber);
        }
    }
}