﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class ParkingLotCollection
    {
        private LinkedList<User> usersList;
        private int userNumber;
        private ParkingLot[] parkingLots;
        public ParkingLotCollection(LinkedList<User> users, ParkingLot[] parkingLots)
        {
            this.userNumber = 0;
            this.usersList = new LinkedList<User>();
            // copy the value of users to this
            foreach (var user in users)
            {
                this.usersList.AddFirst(user);
                this.userNumber++;
            }
            this.parkingLots = new ParkingLot[parkingLots.Length];
            for (int i = 0; i < this.parkingLots.Length; i++)
            {
                this.parkingLots[i] = parkingLots[i];
            }
        }
        public ParkingLot[] GetParkingLots() => this.parkingLots; 
        public void TimeCycle(int hour)
        {
            if (hour > 24 || hour < 0)
                throw new Exception("Time not in range!");
            foreach (ParkingLot p in this.parkingLots)
            {
                p.UpdatePlaces(hour);
            }
        }
        public ParkingLot GetLotByID(int id)
        {
            foreach (ParkingLot parking in this.parkingLots)
            {
                if (parking.GetParkingLotID() == id)
                    return parking;
            }
            throw new Exception("Parking lot not found");
        }
        public ParkingLot GetByAddress(Address addr)
        {
            foreach(ParkingLot parking in this.parkingLots)
            {
                if (parking.GetAddress() == addr.ToString())
                    return parking;
            }
            throw new Exception("Parking lot not found");
        }
        public void SetUserNumber(int userNumber)
        {
            if (userNumber < 0)
                throw new ArgumentException("User number >= 0");
            this.userNumber = userNumber;
        }
        /// <summary>
        /// sets parkingLots of this
        /// </summary>
        /// <param name="parkingLots">new parkingLots</param>
        public void SetParkingLots(ParkingLot[] parkingLots)
        {
            this.parkingLots = new ParkingLot[parkingLots.Length];
            for(int i= 0; i < parkingLots.Length; i++)
            {
                this.parkingLots[i] = parkingLots[i];
            }
        }
        /// <summary>
        /// sets userList
        /// </summary>
        /// <param name="users">new userList</param>
        public void SetUsers(LinkedList<User> users)
        {
            this.usersList = new LinkedList<User>();
            foreach(var user in users)
            {
                this.usersList.AddFirst(user);
            }
        }
        /// <summary>
        /// adds new user to this
        /// </summary>
        /// <param name="newUser"></param>
        public void AddUser(User newUser)
        {
            this.usersList.AddFirst(newUser);
            this.userNumber++;
        }
        /// <summary>
        /// checks if user is in userList of this collection
        /// </summary>
        /// <param name="user">user to find</param>
        /// <returns>true if found, false else</returns>
        public bool UserExists(User user)
        {
            foreach(var tmp in this.usersList)
            {
                if (user.GetID() == tmp.GetID())
                    return true;
            }
            return false;
        }
        /// <summary>
        /// Removes user from list, throws ArgumentException if user is not found
        /// </summary>
        /// <param name="user">user to remove</param>
        public void RemoveUser(User user)
        {
            if (!this.UserExists(user))
                throw new ArgumentException("User not found!");
            this.userNumber--;
            foreach(User u in this.usersList)
            {
                if(u.GetID() == user.GetID())
                {
                    this.usersList.Remove(u);
                    break;
                }
            }
        }
        public User GetUserByID(string id)
        {
            foreach(User u in this.usersList)
            {
                if (u.GetID() == id)
                    return u;
            }
            throw new KeyNotFoundException(String.Format("No user with id {0}", id));
        }
    }
}
