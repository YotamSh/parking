﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingProject
{
    class User
    {
        private string name;
        private string ID;
        private Address address;
        private float payment;
        public User(string name, string ID, Address address, float payment)
        {
            this.name = name;
            this.ID = ID;
            this.address = address;
            this.payment = payment;
        }
        public string GetName()
        {
            return this.name;
        }
        public string GetID()
        {
            return this.ID;
        }
        public Address GetAddress()
        {
            return this.address;
        }
        public float GetPayment()
        {
            return this.payment;
        }
        public void SetName(string name)
        {
            this.name = name;
        }
        public void SetID(string ID)
        {
            this.ID = ID;
        }
        public void SetAddress(Address address)
        {
            this.address = address;
        }
        public void SetPayment(float payment)
        {
            this.payment = payment;
        }
        public void paying()
        {
            this.payment = 0;
        }
        public void addPayment(float payment)
        {
            this.payment += payment;
        }
        public override string ToString()
        {
            return string.Format("Name: {0}\nID: {1}\nAddress: {2}\nPayment: {3}", this.name, this.ID, this.address, this.payment);
        }
    }
}
